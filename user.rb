# == Schema Information
#
# Table name: users
#
#  id                              :integer          not null, primary key
#  email                           :string(255)      not null
#  crypted_password                :string(255)      not null
#  salt                            :string(255)      not null
#  created_at                      :datetime
#  updated_at                      :datetime
#  admin                           :boolean          default(FALSE)
#  stripe_id                       :string(255)
#  last4                           :string(255)
#  name                            :string(255)
#

class User < ActiveRecord::Base
  has_many  :devices, dependent: :destroy

  scope     :admins, -> {where(admin: true)}

  validates :email,           format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :password,        length: { minimum: 8 }
  validates :name, :password, presence: true

  validates :discount, :event_limit, :device_limit, numericality: true

  validates :discount,     length: { in: 0..100, message: 'Discount is invalid' }
  validates :event_limit,  length: { minimum: 0, message: 'Event limit count is invalid' }
  validates :device_limit, length: { minimum: 0, message: 'Device limit count is invalid' }


  def update_card(token)
    customer =
      if stripe_id
        Stripe::Customer.retrieve(stripe_id)
      else
        Stripe::Customer.create(email: email, description: "ID #{id}")
      end
    card = customer.cards.create({ card: token })
    customer.update(default_card: card.id)

    update(stripe_id: cus["id"], last4: card[:last4]) # Maybe cus var is misspelled. Need more info about this variable.
  end

  def update_card(token)
    customer =
      if stripe_id
        Stripe::Customer.retrieve(stripe_id)
      else
        Stripe::Customer.create(email: email, description: "ID #{id}")
      end
    card = customer.cards.create({ card: token })
    customer.update(default_card: card.id)

    # update(stripe_id: customer.id, last4: card[:last4]) # My variant.
    update(stripe_id: cus["id"], last4: card[:last4]) # Mayabe cus var is misspelled. Need more info about this variable.
  end

  def new_device(device_attrs)
    if devices.count >= device_limit
      raise Exceptions::DeviceLimitReached.new("device_limit_reached")
    end
    device.update!(self.devices.build(device_attrs))
  rescue ActiveRecord::StaleObjectError
    reload! and retry
  end

  def add_new_device(device_attrs)
    User.transaction { new_device(device_attrs) }
  end
end
